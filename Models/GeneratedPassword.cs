namespace Somafet.Models
{
    public class GeneratedPassword
    {
        public string Password { get; set; }

        public GeneratedPassword(string password)
        {
            Password = password;
        }
    }
}