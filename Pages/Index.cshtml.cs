﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Somafet;
using Somafet.Abstraction;
using Somafet.Models;
using Somafet.Utils;

namespace Somafet.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IPasswordGenerator _passwordGenerator;

        public IndexModel(ILogger<IndexModel> logger, IPasswordGenerator passwordGenerator)
        {
            _logger = logger;
            _passwordGenerator = passwordGenerator;
        }

        public GeneratedPassword GeneratedPassword;

        public void OnGet()
        {
            GeneratedPassword = new GeneratedPassword(_passwordGenerator.GeneratePassword());
        }

        public void OnPost()
        {
            GeneratedPassword = new GeneratedPassword(_passwordGenerator.GeneratePassword());
        }
    }
}
