﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

let textField;

function copyPasswordToClipboard() {
    if(!textField) {
        console.error("Textfield for generated password not found on body. Cannot copy contents.");
        return;
    }

    textField.select();
    document.execCommand("copy");
}

document.addEventListener('DOMContentLoaded', (event) => {
    textField = document.getElementById('generatedPassword');

    if(!textField) {
        console.error("Textfield for generated password not found on body.");
        
        return;
    }

    textField.select();
})